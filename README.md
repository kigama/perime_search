# # perime_seach_db!

Microservicio de **Perime**. Se encarga de realizar la busqueda de las publicaciones con respecto a diferentes parametros de categorias y productos


# Antes de iniciar

-instalar docker y docker-compose, configurar variables de entorno de go
-ejecutar docker-compose up 
-ejecutar comandos SQL en la base de datos antes de realizar cualquier operación con la API del microserivicio de busquedad. (ver siguiente item)


## Comandos SQL

- Luego de ejecutar el contenedor ejecutar:
	> docker exec -it perime-search-db mysql -p	
- Introducir la contaseña, por decto del contenedor
	> password
- ejecutar las siguientes lineas de código:

       USE perime-search-db;
	
       CREATE TABLE categorys ( id INT AUTO_INCREMENT PRIMARY KEY, Name_Category VARCHAR(50) NOT NULL, Type_Category VARCHAR(50) NOT NULL);


       CREATE TABLE products ( id INT AUTO_INCREMENT PRIMARY KEY, Id_Category INT NOT NULL, Name_Product VARCHAR(50) NOT NULL, Description_Product VARCHAR(50) NOT NULL);

## Operaciones

Se listraran las operaciones:

|Categoria          |Ruta                        |Metodo                        |
|----------------|-------------------------------|-----------------------------|
|Home|`/`            |GET            |
|Obtener categorias|`/categorys`            |GET            |
|Crear una categoria          |`/category`            |POST          |
|Obtener una categoria     |`/category/{id:[0-9]}`|GET            |
|Modificar una categoria         |`/category{id:[0-9]}`|PUT           |
|Eliminar una categoria       |`/category/{id:[0-9]}`|DELETE            |
  




|Producto         |Ruta                        |Metodo                        |
|----------------|-------------------------------|-----------------------------|
|Obtener productos|`/products`            |GET            |
|Crear un producto          |`/product`            |POST          |
|Obtener un producto   |`/product/{id:[0-9]}`|GET            |
|Modificar un producto         |`/product/{id:[0-9]}`|PUT           |
|Eliminar un producto      |`/product/{id:[0-9]}`|DELETE            |


# Librerias

- gitlab.com/kigama/generate_cert: Basado en https://golang.org/src/crypto/tls/generate_cert.go con algunas modificaciones
- github.com/go-sql-driver/mysql: Driver de gestion para MariaDB y MySQL
- github.com/gorilla/mux: Extiende http para manejo simplificado de las peticiones

# SSL
Usando openssl:
- openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key.pem -out cert.pem
De forma predefinida se usara mi libreria generate_cert, este SSL es autofirmado.
